#include "matrix.h"
#include <iostream>

template <typename T>
ostream& operator<<(ostream& os, const Matrix<T>& m){
  for(int r=0; r<m.rows(); r++){
    for(int c=0; c<m.cols(); c++)
      os<<m(r,c)<<' ';
    os<<endl;
  }
  return os;
}

template <typename T>
istream& operator>>(istream& is, Matrix<T>& m){
  int r,c;
  is>>r>>c;
  m.Resize(r,c);
  for(r=0; r<m.rows(); r++)
    for(c=0; c<m.cols(); c++)
      is>>m(r,c);
  return is;    
}

template <typename T>
Matrix<T> operator+(const Matrix<T>& lm, const Matrix<T>& rm){
  if(lm.rows()!=rm.rows() || lm.cols()!=rm.cols()){
    cout << "Invalid operation" << std::endl;
  }
  Matrix<T> ret(lm.rows(),lm.cols());
  for(int r=0; r<lm.rows(); r++)
    for(int c=0; c<lm.cols(); c++)
      ret(r,c)=lm(r,c)+rm(r,c);
  return ret;
}
template <typename T>
Matrix<T> operator-(const Matrix<T>& lm, const Matrix<T>& rm){
  if(lm.rows()!=rm.rows() || lm.cols()!=rm.cols()){
    cout << "Invalid operation" << std::endl;
  }
  Matrix<T> ret(lm.rows(),lm.cols());
  for(int r=0; r<lm.rows(); r++)
    for(int c=0; c<lm.cols(); c++)
      ret(r,c)=lm(r,c)-rm(r,c);
  return ret;
}

template <typename T>
Matrix<T> operator*(const Matrix<T>& lm, const Matrix<T>& rm){
  if(lm.cols()!=rm.rows()){
    cout << "Invalid operation" << std::endl;
  }
  Matrix<T> ret(lm.rows(),rm.cols());
  for(int r=0; r<lm.rows(); r++)
    for(int c=0; c<rm.cols(); c++){
      int s=0;
      for(int k=0; k<lm.cols(); k++)
        s+=lm(r,k)*rm(k,c);
      ret(r,c)=s;
    }
  return ret;
}

template <typename T>
Matrix<T> operator+(const int& a, const Matrix<T>& rm){
  Matrix<T> ret(rm.rows(),rm.cols());
  for(int r=0; r<rm.rows(); r++)
    for(int c=0; c<rm.cols(); c++)
      ret(r,c)=a+rm(r,c);
  return ret;
}
template <typename T>
Matrix<T> operator-(const int& a, const Matrix<T>& rm){
  Matrix<T> ret(rm.rows(),rm.cols());
  for(int r=0; r<rm.rows(); r++)
    for(int c=0; c<rm.cols(); c++)
      ret(r,c)=a-rm(r,c);
  return ret;
}
template <typename T>
Matrix<T> operator*(const int& a, const Matrix<T>& rm){
  Matrix<T> ret(rm.rows(),rm.cols());
  for(int r=0; r<rm.rows(); r++)
    for(int c=0; c<rm.cols(); c++)
      ret(r,c)=a*rm(r,c);
  return ret;
}
template <typename T>
Matrix<T> operator+(const Matrix<T>& lm, const int& a){return a + lm;}
template <typename T>
Matrix<T> operator-(const Matrix<T>& lm, const int& a){return -a+ lm;}
template <typename T>
Matrix<T> operator*(const Matrix<T>& lm, const int& a){return a * lm;}

