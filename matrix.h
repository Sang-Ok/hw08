#ifndef _MATRIX_H_
#define _MATRIX_H_
#include <vector>

using namespace std;

template <typename T>
class Matrix{
public:
  Matrix(int r=0, int c=0) : rows_(r), cols_(c) {values_.resize(r*c);}
  Matrix(const Matrix<T>& mat) : rows_(mat.rows()), cols_(mat.cols()), values_(mat.values_) {}
  int rows()const{return rows_;}
  int cols()const{return cols_;}
//  const int rows()const{return rows_;}
//  const int cols()const{return cols_;}

  T& operator()(int r, int c){return values_[r*cols_+c];}
  Matrix operator+() const{return *this;}
  Matrix operator-() const{
  Matrix mat(cols(),rows());
  for(int r=0; r<rows(); r++)
    for(int c=0; c<cols(); c++)
      mat(r,c)=-(*this)(r,c);
  return mat;
}
  Matrix Transpose() const{
  Matrix mat(rows(),cols());
  for(int r=0; r<rows(); r++)
    for(int c=0; c<cols(); c++)
      mat(c,r)=(*this)(r,c);
  return mat;
}
  
  
protected:
  int rows_, cols_;
  vector<T> values_;
  
};


template <typename T> istream& operator>>(istream& is, Matrix<T>& m);
template <typename T> ostream& operator<<(ostream& os, const Matrix<T>& m);

template <typename T>Matrix<T> operator+(const Matrix<T>& lm, const Matrix<T>& rm);
template <typename T>Matrix<T> operator-(const Matrix<T>& lm, const Matrix<T>& rm);


template <typename T>Matrix<T> operator*(const Matrix<T>& lm, const Matrix<T>& rm);

template <typename T>Matrix<T> operator+(const int& a, const Matrix<T>& rm);
template <typename T>Matrix<T> operator-(const int& a, const Matrix<T>& rm);
template <typename T>Matrix<T> operator*(const int& a, const Matrix<T>& rm);
template <typename T>Matrix<T> operator+(const Matrix<T>& lm, const int& a);
template <typename T>Matrix<T> operator-(const Matrix<T>& lm, const int& a);
template <typename T>Matrix<T> operator*(const Matrix<T>& lm, const int& a);







#endif
