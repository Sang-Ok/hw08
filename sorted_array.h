#ifndef _SORTED_ARRAY_H_
#define _SORTED_ARRAY_H_
#include <string>
#include <set>

template<typename T>
class SortedArray{
public:
  typedef T DataType;
  SortedArray() : data_(NULL),size(0),alloc(0)){}
  void Add(const T& value){
    if(alloc==0) resize(4), return;
    if(size < alloc) data_[++size]=value, return;
    else reserve(new_alloc), Add(value);
  }
  int Find(const T& value){ int i;
    for(i=0; i<size; i++) if(value==data_[i]) break;
    if(i<size) return i;
    else return -1;
  }

protected:
  T* data_;
  int size, alloc;
  void reserve(int new_alloc){
    T* new_data= new T[new_alloc];
    for(int i=0; i<size; i++)
      new_data[i]=data_[i];
    alloc = new_alloc;
    delete[] data_;
    data_=new_data;
  }
}




#endif
